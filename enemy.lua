Enemy = {}
Enemy.__index = Enemy


local maxBalls=10
local ballRadius=15
local speed=50
local rndVel={-1,1}
local id=0

function Enemy.new(x, y, image, level)
  local en = {}
  setmetatable(en, Enemy)
  en.x = x
  en.destroys=false
  en.y = y
  en.radius=ballRadius
  en.id=id+1
  id=id+1
  en.image = love.graphics.newImage(image)
  en.velX=rndVel[math.random(1,2)]*(speed+level)
  en.velY=rndVel[math.random(1,2)]*(speed+level)
  return en
end


function Enemy:move(dt)

  self.x = self.x+dt*self.velX
  self.y = self.y+dt*self.velY

  local centreX= math.floor((self.x+self.radius)/ts)
  local centreY= math.floor((self.y+self.radius)/ts)

  if grid[centreX][centreY]==2 then game.lives=game.lives-1 game.timeout=game.respawn() return  
  else
    local hitX,hitY=centreX,centreY
    local corner=false
    if grid[centreX-1][centreY-1]==1 and  grid[centreX][centreY-1]==0 and grid[centreX-1][centreY]==0 then hitX=centreX-1 hitY=centreY-1 corner=true
    elseif grid[centreX-1][centreY+1]==1 and  grid[centreX-1][centreY]==0 and grid[centreX][centreY+1]==0 then hitX=centreX-1 hitY=centreY+1 corner=true
    elseif grid[centreX+1][centreY+1]==1 and  grid[centreX][centreY+1]==0 and grid[centreX+1][centreY]==0 then hitX=centreX+1 hitY=centreY+1 corner=true
    elseif grid[centreX+1][centreY-1]==1 and  grid[centreX][centreY-1]==0 and grid[centreX+1][centreY]==0 then hitX=centreX+1 hitY=centreY-1 corner=true
    else
      if grid[centreX-1][centreY]==1 then
        hitX=centreX-1
        hitY=centreY
        self:changeXVel(dt)
      elseif grid[centreX+1][centreY]==1  then
        hitX=centreX+1
        hitY=centreY
        self:changeXVel(dt)
      end
      if grid[centreX][centreY-1]==1 then
        hitX=centreX
        hitY=centreY-1
        self:changeYVel(dt)
      elseif grid[centreX][centreY+1]==1  then 
        hitX=centreX
        hitY=centreY+1
        self:changeYVel(dt)
      end  
    end
    if corner==true then 
      self:changeYVel(dt)
      self:changeXVel(dt)
    end
    if self.destroys==true and grid[hitX][hitY]==1 and hitX~=0 and hitX~=tiles_w-1 and  hitY~=0 and hitY~=tiles_h-1 then  grid[hitX][hitY]=0 game.conqueredTiles=game.conqueredTiles-1
    end
  end

end
function Enemy:changeXVel(dt)
  self.velX=self.velX*-1
  self.x = self.x+dt*self.velX
end
function Enemy:changeYVel(dt)
  self.velY=self.velY*-1
  self.y = self.y+dt*self.velY
end
function Enemy:draw()
  love.graphics.draw(self.image, self.x, self.y)
end

function Enemy:collideWithOtherEnemy(other,dt)
  if self.id~=other.id then
    local dx = other.x - self.x
    local dy = other.y - self.y
    if math.sqrt(dx^2 + dy^2) <= self.radius + other.radius then 
      self.velX=self.velX*-1
      self.velY=self.velY*-1
      other.velX=other.velX*-1
      other.velY=other.velY*-1
      self.x = self.x+dt*self.velX
      self.y = self.y+dt*self.velY
      other.x = other.x+dt*other.velX
      other.y = other.y+dt*other.velY
    end
  end
end

function Enemy:generateEnemiesByLevel(level)
  local enemies={}
  local border=2*ts
  local balls=2*game.level
  if balls>maxBalls then balls=maxBalls end
  math.randomseed(os.time())

  for x = 1,balls do
    local rndX=math.random(border,windowWidth-border*2-1)
    local rndY=math.random(border,windowHeight-border*2-1)
    local ball=math.random(4)
    if x%2==0 then  enemies[x]=self.new(rndX,rndY,'assets/balls/ball5.png',level) enemies[x].destroys=true
    else
      enemies[x]=self.new(rndX,rndY,'assets/balls/ball'..ball..'.png',level)
    end
  end

  return enemies
end