require 'snail'
require 'enemy'

windowWidth, windowHeight = love.window.getMode()
ts=10
tiles_w=windowWidth/ts
tiles_h=windowHeight/ts

game={}
game.level=1
game.timer=0
game.passedPercentage=0
game.neededConqueredTiles=tiles_w*tiles_h-game.level*60
game.conqueredTiles=0
game.lives=3
game.bonuses={}
game.bonuses.all={}
game.timeout=0
game.timeoutMessage=''
-- Snail
local mySnail

--Enemies
local enemies={}

--Grid 
grid = {}

local lastMove=''
local lastTile=1
local bonusTypes={'time','points','live'}
local bonusSpawned=false

-- Initialize the grid 
function initializeGrid()
  for x = 0,tiles_w do
    grid[x]={}
    for y = 0,tiles_h do
      if x==0 or y==0 or x==tiles_w-1 or y==tiles_h-1 then
        grid[x][y]=1          
      else
        grid[x][y]=0
      end
    end
  end
end

-- Fills the grid recursively
function grid:fill(x,y)
  if grid[x][y]==0 then grid[x][y]=-1 end
  if grid[x-1][y]==0 then grid:fill(x-1,y) end
  if grid[x+1][y]==0 then grid:fill(x+1,y) end
  if grid[x][y-1]==0 then grid:fill(x,y-1) end
  if grid[x][y+1]==0 then grid:fill(x,y+1) end
end



function grid:drawGroundLayer()
  for x = 0,tiles_w do
    for y = 0,tiles_h do
      if grid[x][y]==0 then goto continue end
      if grid[x][y]==1 then  love.graphics.draw(ground,x*ts,y*ts) end
      if grid[x][y]==2 then love.graphics.draw(trail,x*ts,y*ts) 
      end
      ::continue::
    end
  end
end

function isTileChanged(prevX,prevY,currX,currY)
  if prevX~=currX or prevY~=currY then
    return true
  end
end

function circleCollision(circle1, circle2)
  local dx = circle2.x - circle1.x
  local dy = circle2.y - circle1.y
  return math.sqrt(dx^2 + dy^2) <= circle1.radius + circle2.radius
end

function game.bonuses.spawnBonus()
  local bonus={}
  bonus.type=bonusTypes[math.random(3)]
  bonus.timer=10
  bonus.image=love.graphics.newImage("assets/"..bonus.type..'.png') 
  bonus.x=math.random(bonus.image:getWidth()/2,windowWidth-bonus.image:getWidth()*1.5)
  bonus.y=math.random(bonus.image:getHeight()/2,windowHeight-bonus.image:getHeight()*1.5)
  table.insert(game.bonuses.all,bonus)
end

function game.bonuses.update(dt)
  for k,v in ipairs(game.bonuses.all) do
    v.timer=v.timer-dt
    if v.timer<=0 then table.remove(game.bonuses.all,k)
    elseif v.x<mySnail.hitbox.x2 and v.x+30>mySnail.hitbox.x1 and v.y<mySnail.hitbox.y2 and v.y+30 >mySnail.hitbox.y1 then 
      if v.type=='time' then game.timer=game.timer+10 
      elseif v.type=='live' then game.lives=game.lives+1
      elseif v.type=='points' then game.neededConqueredTiles=game.neededConqueredTiles-200
      end
      table.remove(game.bonuses.all,k)
    end
  end
end

function game.bonuses.draw()
  for i,v in ipairs(game.bonuses.all) do 
    love.graphics.draw(v.image,v.x,v.y)
  end
end

function game.load()
  game.state='game'

  game.level=1
  game.lives=3

  game.bonuses.all={}

  game.loadLevel(game.level)

  messageFont=love.graphics.newFont('assets/fonts/message.ttf',32)
  font  = love.graphics.newImageFont("assets/fonts/scoresFont.png",
    " abcdefghijklmnopqrstuvwxyz" ..
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
    "123456789.,!?-+/():;%&`'*#=[]\"")
  love.graphics.setFont(font)

end

function game.loadLevel(level)
  if game.level>1 then
    game.timeout=3
    game.timeoutMessage="Level Up"
  end
  mySnail = Snail.new()
  lastMove=''
  game.timer=30+game.level*30
  initializeGrid()
  background = love.graphics.newImage("assets/tiles/water.jpg")
  ground = love.graphics.newImage("assets/tiles/sand.jpg")  
  trail = love.graphics.newImage("assets/tiles/trail.png")
  game.conqueredTiles=0
  game.passedPercentage=0

  enemies=Enemy:generateEnemiesByLevel(game.level)
end

function game.respawn()
  mySnail = Snail.new()
  lastMove=''
  if game.timer<=0 then game.timer=30+game.level*30 game.timeoutMessage="Time's up" 
  else 
    game.timeoutMessage="You died"
  end
  for x = 0,tiles_w do
    for y = 0,tiles_h do
      if grid[x][y]==2 then grid[x][y]=0 end
    end
  end
  return 2
end

function game.draw()
  --Draw background
  for i = 0, love.graphics.getWidth() / background:getWidth() do
    for j = 0, love.graphics.getHeight() / background:getHeight() do
      love.graphics.draw(background, i * background:getWidth(), j * background:getHeight())
    end
  end

  grid:drawGroundLayer()

  mySnail:draw()

  for i,v in ipairs(enemies) do v:draw() end  

  game.bonuses.draw()

--Draw lives and scores
  local textColor = {4,192,203}
  local lives = {textColor, 'Lives:'..game.lives}
  local percetns={textColor,'%'..game.passedPercentage}  
  local level = {textColor, 'Level:'..game.level}
  local time = {textColor, 'Time left:'..math.floor(game.timer)}

  love.graphics.setFont(font)
  love.graphics.printf(lives,font, 25, 25,  windowWidth, 'left')
  love.graphics.printf(percetns,font,25, 40, windowWidth, 'left')
  love.graphics.printf(level,font,25, 55, windowWidth, 'left')  
  love.graphics.printf(time,font,-25, 25, windowWidth, 'right')

  local textYPos = windowHeight / 2 

  if game.timeout>0 then
    love.graphics.setFont(messageFont)
    love.graphics.printf(game.timeoutMessage, 0, textYPos, windowWidth, "center")
  end
end

function game.update(dt)
  game.timeout=game.timeout-dt
  if game.timeout<=0 then
    if game.lives==0 then game.state='dead' return game.state end
    if game.timer<=0 then game.lives=game.lives-1 game.timeout=game.respawn() return game.state end
    game.bonuses.update(dt)

    for i = 1,table.getn(enemies) do
      enemies[i]:move(dt)
    end
    for i = 1,table.getn(enemies) do
      for j = 1,table.getn(enemies) do
        enemies[i]:collideWithOtherEnemy(enemies[j],dt)
      end
    end

    local previousSnailTileX=math.floor(mySnail.hitbox.x1/10 )
    local previousSnailTileY=math.floor(mySnail.hitbox.y1/10 )

    if love.keyboard.isDown("down") or love.keyboard.isDown("s")  then
      if lastMove=='up' and lastTile==2 then lastMove='up' 
      else lastMove='down' end

    elseif love.keyboard.isDown("up") or love.keyboard.isDown("w") then
      if lastMove=='down' and lastTile==2 then lastMove='down' 
      else lastMove='up' end

    elseif love.keyboard.isDown("left") or love.keyboard.isDown("a") then
      if lastMove=='right' and lastTile==2 then lastMove='right' 
      else lastMove='left' end

    elseif love.keyboard.isDown("right") or love.keyboard.isDown("d")  then
      if lastMove=='left' and lastTile==2 then lastMove='left' 
      else lastMove='right' end

    end

    if lastMove=='down' then
      mySnail:moveY(mySnail.speed * dt)
    end
    if  lastMove=='up' then
      mySnail:moveY(-mySnail.speed * dt)

    end
    if  lastMove=='left' then
      mySnail:moveX(-mySnail.speed * dt)

    end
    if lastMove=='right' then
      mySnail:moveX(mySnail.speed * dt)
    end

    local currentSnailTileX=math.floor(mySnail.hitbox.x1/10)
    local currentSnailTileY=math.floor(mySnail.hitbox.y1/10)



    if isTileChanged(previousSnailTileX,previousSnailTileY,currentSnailTileX,currentSnailTileY) then

      if grid[currentSnailTileX][currentSnailTileY]==2 then game.lives=game.lives-1 game.timeout=game.respawn() return game.state
      elseif grid[currentSnailTileX][currentSnailTileY]==0 then grid[currentSnailTileX][currentSnailTileY]=2 
      elseif  grid[currentSnailTileX][currentSnailTileY]==1 and lastTile==1 then lastMove='' lastTile=1
      elseif grid[currentSnailTileX][currentSnailTileY]==1 and lastTile==2 then 
        lastMove=''
        for x = 1,table.getn(enemies) do
          grid:fill(math.floor(enemies[x].x/ts+1),math.floor(enemies[x].y/ts+1))
        end
        game.conqueredTiles=0

        for x = 0,tiles_w do
          for y = 0,tiles_h do
            if grid[x][y]==-1 then grid[x][y]=0
            else 
              grid[x][y]=1
              game.conqueredTiles=game.conqueredTiles+1
            end
          end
        end


      else
        lastMove=''
      end
      lastTile=grid[currentSnailTileX][currentSnailTileY]
    end
    game.passedPercentage=math.floor((game.conqueredTiles*100)/game.neededConqueredTiles)
    if game.passedPercentage>=100 then game.level=game.level+1 game.loadLevel(game.level) end
    game.timer=game.timer-dt
    if math.floor(game.timer)%30==0 and not bonusSpawned then game.bonuses.spawnBonus() bonusSpawned=true 
    elseif math.floor(game.timer)%30~=0 then bonusSpawned=false
    end
  end
  return game.state
end

