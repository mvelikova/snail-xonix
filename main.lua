require "start"
require "game"
require "endgame"

local gameState
local newState

function love.load()
  -- Backgroundsound
  gameState = 'start'
  newState = 'start'
  start.load()
end

function love.update(dt)
  if gameState == "game" and gameState == newState then
    newState= game.update(dt)
  end

  if gameState ~= newState then
    gameState = newState
    if newState == "start" then
      start.load()
    elseif newState == "game" then
      game.load()
    elseif newState == "dead" then
      endgame.load()
    end
  end
end

function love.draw()
  if gameState == "start" then
    start.draw()
  elseif gameState == "game" then
    game.draw()
  elseif gameState == "dead" then
    endgame.draw()
  end
end

function love.keypressed(key, _, isRepeat)
  if not isRepeat then
    if key == "escape" then
      if gameState == "start" then
        love.event.quit()
      elseif gameState == "game" then
        newState = "start"
      elseif gameState == "dead" then
        newState = "start"
      end
    end
    if key == 'return' then
      if gameState == 'start' or gameState == 'dead' then
        newState = 'game'
      end
    end
  end
end

