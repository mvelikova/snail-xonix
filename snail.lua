Snail = {}
Snail.__index = Snail
local brick_width=10
local brick_height=10
function Snail.new()
  local sn = {}
  setmetatable(sn, Snail)

  sn.imageLeft = love.graphics.newImage("assets/snail/left.png")
  sn.imageRight = love.graphics.newImage("assets/snail/right.png")
  sn.imageUp = love.graphics.newImage("assets/snail/up.png")
  sn.imageDown = love.graphics.newImage("assets/snail/down.png")

  -- 0 = right, 1 = down, 2 = left, 3 = up
  sn.facingDirection = 3
  sn.snailWidth, sn.snailHeight = sn:getCurrentImage():getDimensions()
  sn.x=love.graphics.getWidth()/2-sn.snailWidth/2
  sn.y=love.graphics.getHeight()-sn.snailHeight/2
  sn.hitbox={}
  sn.hitbox.x1=sn.x+30
  sn.hitbox.x2=sn.x+40
  sn.hitbox.y1=sn.y+30
  sn.hitbox.y2=sn.y+40
  sn.speed=200


  return sn
end


function Snail:moveX(p)

  -- 0 = right, 1 = down, 2 = left, 3 = up
  if p < 0 then
    self.facingDirection = 2
  else
    self.facingDirection = 0
  end

  self.x = self.x + p

  -- Stop at borders
  if self.x < -self.snailWidth/2 then
    self.x = -self.snailWidth/2
  end
  if self.x  > love.graphics.getWidth()- self.snailWidth/2 then
    self.x = love.graphics.getWidth()-self.snailWidth/2 
  end
  self.hitbox.x1=self.x+30
  self.hitbox.x2=self.x+40
if self.hitbox.x1<0 then self.hitbox.x1=0 end
if self.hitbox.x2>windowWidth then self.hitbox.x2=windowWidth end

end

function Snail:moveY(p)


  -- 0 = right, 1 = down, 2 = left, 3 = up
  if p < 0 then
    self.facingDirection = 3
  else
    self.facingDirection = 1
  end

  self.y = self.y + p

  -- Stop at borders
  if self.y < -self.snailHeight/2 then
    self.y = -self.snailHeight/2
  end
  if self.y> love.graphics.getHeight() - self.snailHeight/2 then
    self.y = love.graphics.getHeight() - self.snailHeight/2
  end
  self.hitbox.y1=self.y+30
  self.hitbox.y2=self.y+40
  if self.hitbox.y1<0 then self.hitbox.y1=0 end
if self.hitbox.y2>windowHeight then self.hitbox.y2=windowHeight end
end

function Snail:getCurrentImage()
  -- 0 = right, 1 = down, 2 = left, 3 = up
  if self.facingDirection == 0 then
    return self.imageRight
  end
  if self.facingDirection == 1 then
    return self.imageDown
  end
  if self.facingDirection == 2 then
    return self.imageLeft
  end
  if self.facingDirection == 3 then
    return self.imageUp
  end
end

function Snail:draw()

  love.graphics.draw(self:getCurrentImage(), self.x, self.y)

end
