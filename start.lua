start = {}

local startFont
local smallFont
local coloredText
local descText1
local snailImg

function start.load()
  snailImg=love.graphics.newImage('assets/snail/sadSnail.png')

  startFont = love.graphics.setNewFont('assets/fonts/startFont.ttf',32)
  smallFont = love.graphics.setNewFont('assets/fonts/startFont.ttf',20)

  local textColor = {255,192,203}
  coloredText = {textColor, "Snail Xonix"}
  descText1={textColor,"Press ENTER to start!"}

  love.graphics.setBackgroundColor(0,0,0)
end

function start.draw()
  local windowWidth, windowHeight = love.window.getMode()
  love.graphics.draw(snailImg,windowWidth/2-snailImg:getWidth()/2,80)

  local textYPos = windowHeight / 2 
  love.graphics.setFont(startFont)
  love.graphics.printf(coloredText, 0, textYPos, windowWidth, "center")

  love.graphics.setFont(smallFont)
  love.graphics.printf(descText1, 0, textYPos+80, windowWidth, "center")

end

return start
