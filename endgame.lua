endgame = {}

local gameOverFont
local descFont

function endgame.load()
  gameOverFont= love.graphics.setNewFont('assets/fonts/startFont.ttf',32)
  descFont=love.graphics.newImageFont("assets/fonts/scoresFont.png",
    " abcdefghijklmnopqrstuvwxyz" ..
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
    "123456789.,!?-+/():;%&`'*#=[]\"")
  textColor = {255,192,203}
  gameOverText={textColor,'Game over'}  
  descText={textColor,'Press ESC to go back and start again'}
  love.graphics.setBackgroundColor(0,0,0)

end
function endgame.draw()

  local textYPos = windowHeight / 2 
  love.graphics.setFont(gameOverFont)
  love.graphics.printf(gameOverText, 0, textYPos, windowWidth, "center")
  love.graphics.setFont(descFont)
  love.graphics.printf(descText, 0, textYPos+80, windowWidth, "center")
end

return endgame
